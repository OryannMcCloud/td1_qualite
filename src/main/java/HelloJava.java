/**
 * Ceci est une classe de test qui affiche cancan par défaut
 * @author oryann prochaska
 */
public class HelloJava {
	public static void afficherCancan(){
		System.out.println("cancan");
	}

	public static void main(String[] args) {
		afficherCancan();
	}
}
